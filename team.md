[Home](index.md) | [Project](Project_Idea.md) | Team | [Vocabulary](Terminologies.md)
____
# My Team

**Meet the PlantTechs Team**

**Team Members:**

1. **Tshering**

![](https://www.pikpng.com/pngl/b/451-4510273_andy-copps-cartoon-passport-size-photo-cartoon-clipart.png)


   - Age: 17
   - Hobbies: Gardening, painting, hiking
   - What I hope to learn: I'm excited to learn more about Arduino programming and how to integrate technology into environmental projects. I'm also eager to improve my teamwork and communication skills.
   - Contribution to the team: I have a knack for creative problem-solving and attention to detail. I'll be focusing on designing the physical setup of our plant monitoring system and ensuring it's aesthetically pleasing.

2. **Sonam**
![](https://www.pikpng.com/pngl/b/451-4510273_andy-copps-cartoon-passport-size-photo-cartoon-clipart.png)


   - Age: 16
   - Hobbies: Playing guitar, coding, soccer
   - What I hope to learn: I'm interested in gaining hands-on experience with hardware components and learning how to troubleshoot technical issues. I also want to improve my leadership skills.
   - Contribution to the team: With my coding skills, I'll take charge of programming the Arduino and implementing the necessary algorithms for our plant monitoring system. I'll also facilitate peer learning sessions to share my knowledge with the team.

3. **Dorji**
![](https://www.pikpng.com/pngl/b/451-4510273_andy-copps-cartoon-passport-size-photo-cartoon-clipart.png)

   - Age: 17
   - Hobbies: Photography, baking, playing piano
   - What I hope to learn: I'm looking forward to learning more about sensor technologies and how they can be used in environmental monitoring. I also hope to enhance my project management skills.
   - Contribution to the team: I'll be responsible for researching and selecting the appropriate sensors for our project. Additionally, I'll organize weekly team meetings to ensure everyone is on track and facilitate peer learning discussions.

4. **Tenzin**
![](https://www.pikpng.com/pngl/b/451-4510273_andy-copps-cartoon-passport-size-photo-cartoon-clipart.png)

   - Age: 16
   - Hobbies: Woodworking, basketball, volunteering at animal shelters
   - What I hope to learn: I'm eager to learn about the practical applications of technology in addressing environmental issues. I also want to improve my teamwork and collaboration skills.
   - Contribution to the team: With my hands-on skills, I'll assist in building the physical components of our plant monitoring system. I'll also organize skill-sharing sessions where team members can teach each other new techniques and concepts.

**Peer Learning Plan:**

To ensure effective peer learning within our team, we have devised a structured approach:

1. **Task Division:** We will divide the project tasks based on each member's strengths and interests, ensuring that everyone has a chance to contribute meaningfully.

2. **Weekly Skill-Sharing Sessions:** Each week, one team member will lead a skill-sharing session where they will teach the rest of the team about a relevant topic or technique related to our project.

3. **Regular Feedback:** We will provide constructive feedback to each other on our contributions to the project, highlighting areas for improvement and areas of strength.

4. **Collaborative Problem-Solving:** Whenever a team member encounters a challenge, we will work together to find solutions, drawing on the diverse expertise within our group.

5. **Open Communication:** We will maintain open and honest communication, encouraging everyone to ask questions, share ideas, and seek help when needed.

With these strategies in place, we are confident that our team will foster a supportive and collaborative learning environment where everyone can thrive.

___

[Home](index.md) | [Project](Project_Idea.md) | Team | [Vocabulary](Terminologies.md)
____