[Home](index.md) | [Project](Project_Idea.md) | [Team](team.md) | Vocabulary
____
# Terms to Know:

**PlantTechs Project Vocabulary**

Welcome to the PlantTechs Project Vocabulary page! Here, we'll be recording new terminology and vocabulary related to our project on developing a plant monitoring system using Arduino technology.

1. **Arduino**: An open-source electronics platform based on easy-to-use hardware and software. [Learn more about Arduino](https://www.arduino.cc/)

2. **Sensor**: A device that detects or measures a physical property and converts it into an analog or digital signal.

3. **Actuator**: A device that converts an electrical signal into physical action or movement.

4. **Soil Moisture Sensor**: A sensor used to measure the moisture content in soil.

5. **Microcontroller**: A small computer on a single integrated circuit containing a processor core, memory, and programmable input/output peripherals.

6. **Embedded Programming**: Programming specifically for embedded systems, which are small computing devices designed for specific tasks or functions.

7. **Circuit Design**: The process of creating a schematic diagram that represents the connections and components of an electronic circuit.

8. **Algorithm**: A set of instructions or rules followed by a computer to solve a problem or perform a task.

9. **Serial Communication**: The process of sending data one bit at a time over a communication channel, such as a serial port or USB connection.

10. **Data Logging**: The process of recording data over time for analysis or monitoring purposes.

___

[Home](index.md) | [Project](Project_Idea.md) | [Team](team.md) | Vocabulary
____