[Home](index.md) | Project | [Team](team.md) Home | [Vocabulary](Terminologies.md)
_______
# Project Idea : Spider-Bot


Explain your project idea with sketches or anything possible.

![Project Idea](./images/final%20_display.png)

Some explaination on project and sketchs



![](https://fabacademy.org/2022/labs/bhutan/students/anith-ghalley/images/Project_Sketch.jpg)


## Research

Movement of the spider will mimic the movement of ants limbs Because spiders are usually eight-legged, so to reduce the number of servos being used the spider will have 3 pairs of legs, each legs will have two servos. The locumotion for this spider bot will be based on the locumotion of other three legged insects.

![](./images/ant_leg.png)

Locumotion of three-legged insects: While walking usually they either move one or two limbs at a time but ​​ When running, an insect moves three legs simultaneously. This is the tripod gait, so called because the insect always has three legs in contact with the ground: front and hind legs on one side of the body and middle leg on the opposite side.

## Working Concept

For a flying drons to reach its current stage advancement where it can fly on its own. But before that it was tried out with various human interventation menthod. Therefore I think that for the crawler drones to to advance, I want to keep the drones to a certain range of control from the user.

![](./images/anglename.png)

The spider would move forward when there is no obstacles in front of it and when ever there is an obstacles at a range less than 50 cm, The crawler will randomly find a direction with no obstacle inside the front range of the ultrasonic sensor. The crawler shoud also be able to connect to the given network so that at times human intervention can also be provided.

![](./images/controllor.jpg)

## Components Required :

1. Power Supply. 
2. Micro-Controller 
3. Ultrasonic Sensor 
4. Servo Motors

Thank You

_____

[Home](index.md) | Project | [Team](team.md) | [Vocabulary](Terminologies.md)
___